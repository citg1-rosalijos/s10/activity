package com.zuitt.discussion;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.*;

@SpringBootApplication
@RestController
public class ActivityApplication {

	public static void main(String[] args) {
		SpringApplication.run(ActivityApplication.class, args);
	}

	// Get all users
	@GetMapping("/users")
	public String getUsers() {
		return "All users retrieved.";
	}

	// Create user
	@PostMapping("/users")
	public String createUser() {
		return "New user created.";
	}

	// Get a specific user
	@GetMapping("/users/{userId}")
	public String getUser(@PathVariable Long userId) {
		return "View details of user " + userId + ".";
	}

	// Delete a user
	@DeleteMapping("/users/{userId}")
	public String deleteUser(@PathVariable Long userId, @RequestHeader(value="Authorization") String user) {
		String message = "The user " + userId + " has been deleted.";
		if(user == "")
			message = "Unauthorized access.";
		return message;
	}

	// Update user
	@PutMapping("/users/{userId}")
	public User updateUser(@PathVariable Long userId, @RequestBody User user) {
		return user;
	}

}
